import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*
  Generated class for the SrcProvidersTestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface WeatherDescription {
  id: number,
  main: string,
  description: string,
  icon: string
}

export interface WeatherMain {
  temp: number,
  pressure: number,
  humidity: number,
  temp_min: number,
  temp_max: number
}

export interface WeatherData {
    weather: [WeatherDescription],
    main: WeatherMain
    //tempMax: number,
    //tempMin: number,
    //tempCurrent: number
}

@Injectable()
export class WeatherProvider {

  private weatherURL: string = "api.openweathermap.org/data/2.5/weather?lat=37.391411&lon=-5.959186&units=metric&appid=1450a7457f216b229e6b32bb81b61db2&lang=es";

  constructor(public http: HttpClient) { }

  public getWeatherData() {

    return this.http.get<WeatherData>( this.weatherURL )
    .map ( res => {
      return res;
    })
  }

    /*
    return this.http.get(this.weatherURL)
      .map( res => {

        //console.log("Datos: " + res.weather[0].description);
        //data.description = res.json().weather.description;
        //data.tempCurrent = res.json().weather.main.temp;
        //data.tempMax = res.json().weather.main.temp_max;
        //data.tempMin = res.json().weather.main.temp_min;

        //return data;
      })
      */

}
