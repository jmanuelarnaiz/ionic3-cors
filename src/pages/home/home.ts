import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider, WeatherData } from '../../providers/weather.provider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private description: string;
  private temp_max: number;
  private temp_min: number;
  private temp_current: number;
  private humidity: number;

  constructor(public navCtrl: NavController, private _weatherProvider: WeatherProvider ) {
  }

  private ionViewDidLoad() {

  }

  private getWeatherData(): void {
    console.log("Recupero datos del tiempo");
    this._weatherProvider.getWeatherData().subscribe( data => {
      this.description = data.weather[0].description;
      this.temp_max = data.main.temp_max;
      this.temp_min = data.main.temp_min;
      this.temp_current = data.main.temp;
      this.humidity = data.main.humidity;
    });
  }

  private reset(): void {
    this.description = null;
    this.temp_max = null;
    this.temp_min = null;
    this.temp_current = null;
    this.humidity = null;
  }

}
